<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'name','email','phone','applicant_id'
    ];
    public function applicant()
    {
    	return $this->belongsTo('App\applicant');
    }
}
