<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
    protected $fillable = [
        'user_id','activity_id','description','status_id','limitdate'
    ];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function activity()
    {
    	return $this->belongsTo('App\Activity');
    }
    public function status()
    {
    	return $this->belongsTo('App\status');
    }
}
