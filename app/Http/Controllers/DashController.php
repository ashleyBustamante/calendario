<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Department;
use App\Slas;
use App\Activity;
use App\Task;

class DashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dash');
    }

    public function getGraph1($id)
    {
        $slasQ = Slas::all();
        $slas = $slasQ->pluck('description');
        $departmentsQ= Department::all();
        $departments=$departmentsQ->pluck('name');

        $results = DB::table('activities')
                    ->join('slas','activities.sla_id','=','slas.id')
                    ->join('departments','activities.department_id','=','departments.id')
                    ->select(DB::raw('COUNT(activities.id) as S, departments.name,slas.description'))
                    ->where('status_id',$id)
                    ->groupBy('departments.name','slas.description')
                    ->get();

        $series = collect([]);

        foreach ($departmentsQ as $department) {
            $data = collect([]);
            foreach($slasQ as $sla){
                $filtered = $results->where('name',$department->name)
                                ->where('description',$sla->description)
                                ->first();

                if(is_null($filtered)){
                    $data->push(0);
                }
                else{
                    $data->push(intval($filtered->S));
                }
            }

            $serie = [
                'name'=>$department->name,
                'type'=>'bar',
                'barGap'=>0,
                'data'=>$data
            ];

            $series->push($serie);
        }

        return compact('series','departments','slas','results');
    }
}
