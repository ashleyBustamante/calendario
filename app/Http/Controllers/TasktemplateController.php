<?php

namespace App\Http\Controllers;


use App\Template;
use App\Tasktemplate;
use Illuminate\Http\Request;

class TasktemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('tasktemplate.create');
        $data = $p->r ? Tasktemplate::with('template')->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $p = $this->getPermission('tasktemplate.create');
        return view('tasktemplate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Tasktemplate::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Tasktemplate $tasktemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= Tasktemplate::findOrFail($id);
        $templates= Template::all();
        $view = view('tasktemplate.edit',['data'=>$data,'templates'=>$templates])->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $template=Template::findOrFail($id);
       return (int)$template->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template=Template::findOrFail($id);
        return (int)$template->delete();
    }
}
