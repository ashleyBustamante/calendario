<?php

namespace App\Http\Controllers;

use App\Slarole;
use App\slas;
use App\Role;
use Illuminate\Http\Request;

class SlaroleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('slarole.create');
        $data = $p->r ? Slarole::with(['slas','role'])->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $p = $this->getPermission('slarole.create');
        return view('slarole.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Slarole::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Slarole $slarole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= Slarole::findOrFail($id);
        $slas= slas::all();
        $roles= Role::all();
        $view = view('slarole.edit',['data'=>$data,'slas'=>$slas,'roles'=>$roles])->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slasroles=Slasrole::findOrFail($id);
       return (int)$slasroles->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slasroles=Slasrole::findOrFail($id);
        return (int)$slasroles->delete();
    }
}
