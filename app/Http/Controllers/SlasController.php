<?php

namespace App\Http\Controllers;

use App\Slas;
use Illuminate\Http\Request;

class SlasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('slas.create');
        $data = $p->r ? Slas::all() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $p = $this->getPermission('slas.create');
        return view('slas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Slas::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Slas $slas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slas = Slas::where('id',$id)->first();
        $view = view('slas.edit',compact('slas'))->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slas = Slas::where('id',$id)->first();
        return (int)$slas->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slas = Slas::where('id',$id)->first();
        return (int)$slas->delete();
    }
}
