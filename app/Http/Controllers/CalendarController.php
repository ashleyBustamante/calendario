<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Slas;
use App\Activity;
use App\Task;
use Auth;

class CalendarController extends Controller
{
    public function index()
    {
        return view('calendar.index');
    }

    public function getEvents()
    {
        $activities = Activity::where('user_id',Auth::user()->id)->with('sla')->get();
        $tasks = Task::where('user_id',Auth::user()->id)->with('activity')->get();
        $events=collect([]);
         foreach ($activities as $activity) {
           $events->push([
            '_id'=>$activity->id,
            'type'=>1,
            'title'=>$activity->name,
            'start'=>$activity->limitdate,
            'description'=>$activity->descripion,
            'allDay'=>false,
            'editable'=>false,
            'color'=>'#'.$activity->sla->color
          ]);
        }

        foreach ($tasks as $task) {
            $sla = Slas::where('id',$task->activity->sla_id)->first();
           $events->push([
            '_id'=>$task->id,
            'type'=>2,
            'color'=>'#'.$sla->color,
            'title'=>$task->description,
            'start'=>$task->limitdate,
            'allDay'=>false,
            'editable'=>false
          ]);
        }

        return $events;
    }
}
