<?php

namespace App\Http\Controllers;

use App\task;
use App\User;
use App\Activity;
use App\status;
use App\tasktemplate;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_filter($id)
    {
        $p = $this->getPermission('task.create');
        $data = $p->r ? task::where('activity_id',$id)->with(['user','activity','status'])->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }


    public function index()
    {
        $p = $this->getPermission('task.create');
        $data = $p->r ? task::with(['user','activity','status'])->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $p = $this->getPermission('task.create');
        return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        //'user_id','activity_id','description','status_id','limitdate'
        if(isset($data['template_id'])){
            $template = tasktemplate::where('template_id',$data['template_id'])->get();
            foreach ($template as $t) {
                Task::create([
                    'user_id'=>1,
                    'activity_id'=>$data['activity_id'],
                    'description'=>$t->name,
                    'status_id'=>1,
                    'limitdate'=>Carbon::now()
                ]);
            }
        }
        else{
            return Task::create($request->all());    
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= Task::findOrFail($id);
        $users= User::all();
        $activities=activity::all();
        $statuses= status::all();
        $view = view('task.edit',['data'=>$data, 'users'=>$users,'activities'=>$activities, 'statuses'=>$statuses] )->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $task= Task::findOrFail($id);
       return (int)$task->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task= Task::findOrFail($id);
        return (int)$task->delete();
    }
}
