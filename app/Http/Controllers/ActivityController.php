<?php

namespace App\Http\Controllers;

use App\Activity;
use App\User;
use App\status;
use App\priority;
use App\request_type;
use App\slas;
use App\organization;
use App\subcategory;
use App\department;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $p = $this->getPermission('activity.create');
        $data = $p->r ? Activity::with(['user','status','priority','request','sla','organization','subcategory','department'])->get() : [];
        foreach($data as $d)
        $d['p'] = array('a'=>false,'e'=>$p->u,'d'=>$p->d);
        return datatables()->of($data)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $p = $this->getPermission('activity.create');
        return view('activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Activity::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= Activity::findOrFail($id);
        $users= User::all();
        $statuses= status::all();
        $priorities= priority::all();
        $requests= request_type::all();
        $slas= slas::all();
        $organizations = organization::all();
        $subcategories= subcategory::all();
        $departments= department::all();
        $view = view('activity.edit',['data'=>$data, 'users'=>$users, 'statuses'=>$statuses,'priorities'=>$priorities,'requests'=>$requests,'slas'=>$slas,'organizations'=>$organizations,'subcategories'=>$subcategories,'departments'=>$departments] )->render();
        return compact('view');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $activity= Activity::findOrFail($id);
       return (int)$activity->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity= Activity::findOrFail($id);
        return (int)$activity->delete();
    }
}
