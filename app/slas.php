<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slas extends Model
{
     protected $fillable = [
        'description','color'
    ];
}
