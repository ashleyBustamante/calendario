<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasktemplate extends Model
{
    protected $fillable = [
        'template_id','name'
    ];
    public function template()
    {
    	return $this->belongsTo('App\template');
    }
}
