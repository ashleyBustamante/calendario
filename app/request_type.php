<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class request_type extends Model
{
  protected $fillable = [
        'request_type'
    ];
}
