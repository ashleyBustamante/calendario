<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'name', 'description','user_id','status_id','priority_id','requestdate','limitdate','accepteddate','request_id','sla_id','organization_id','subcategory_id','department_id'
    ];

	public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function status()
    {
    	return $this->belongsTo('App\status');
    }
    public function priority()
    {
    	return $this->belongsTo('App\priority');
    }
    public function request()
    {
    	return $this->belongsTo('App\request_type');
    }
    public function sla()
    {
    	return $this->belongsTo('App\slas');
    }
    public function organization()
    {
    	return $this->belongsTo('App\organization');
    }
    public function subcategory()
    {
    	return $this->belongsTo('App\subcategory');
    }
     public function department()
    {
        return $this->belongsTo('App\department');
    }
}
