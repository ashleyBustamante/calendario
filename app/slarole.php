<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slarole extends Model
{
      protected $fillable = [
        'sla_id','role_id'
    ];
    public function slas()
    {
    	return $this->belongsTo('App\slas');
    }
    public function roles()
    {
    	return $this->belongsTo('App\role');
    }

}
