<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicant extends Model
{
    protected $fillable = [
        'name','lastnamep','lastnamem','email'
    ];
}
