<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test',function () {
    $permissions = App\Permission_Role::where('role_id',1)->with('permission')->get();
    $parentMenus = Illuminate\Support\Collection::make();
    $childMenus = Illuminate\Support\Collection::make();
    foreach($permissions as $permission){
        if($permission['permission']['parent'] == -1)
            $parentMenus->push($permission['permission']);
        else
            $childMenus->push($permission['permission']);
    }
    return compact('parentMenus','childMenus');
});

Auth::routes();
Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/login',function () {
    return view('auth.login');
})->name('login');

Route::get('/admin/dash', 'DashController@index')->name('dash');
Route::resource('/admin/user','UserController');
Route::resource('/admin/role','RoleController');
Route::put('/admin/permission/priority','PermissionController@updatePriority')->name('permission.priotity');
Route::resource('/admin/permission','PermissionController');
Route::resource('/admin/department','DepartmentController');
Route::get('/icons','PermissionController@icons');
Route::get('/admin/calendar','CalendarController@index')->name('calendar');
Route::get('/admin/calendar/events','CalendarController@getEvents');
Route::resource('/admin/applicant','ApplicantController');
Route::resource('/admin/organization','OrganizationController');
Route::resource('/admin/request_type','RequestTypeController');
Route::resource('/admin/slas','SlasController');
Route::resource('/admin/priority','PriorityController');
Route::resource('/admin/status','StatusController');
Route::resource('/admin/category','CategoryController');
Route::resource('/admin/subcategory','SubcategoryController');
Route::resource('/admin/slaroles','slaroleController');
Route::resource('/admin/activity','ActivityController');
Route::resource('/admin/task','TaskController');
Route::resource('/admin/template','TemplateController');
Route::resource('/admin/tasktemplate','TasktemplateController');
Route::get('/admin/graph1/{id}','DashController@getGraph1');
Route::get('/admin/ftask/{id}','TaskController@index_filter');