<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('user_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('priority_id')->unsigned();
            $table->date('requestdate');
            $table->date('limitdate');
            $table->date('accepteddate');
            $table->integer('request_id')->unsigned(); //via
            $table->integer('sla_id')->unsigned();
            $table->integer('organization_id')->unsigned();
            $table->integer('subcategory_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
