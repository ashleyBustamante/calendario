@extends('layouts.admin')
@section('scripts')
<script src="/js/task.js"></script>
@stop
@section('content')
@if($crud->c)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-book"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Crear Nueva Tarea
                        </h3>
                    </div>
                </div>
            </div>
            <form action="" id="new">
                <div class="m-portlet__body">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="activity_id" class="form-control m-select2" required id="activity">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="template_id" class="form-control m-select2"  id="template">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="user_id" class="form-control m-select2"  id="user">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class=" form-control form-control-danger m-input"  placeholder="Descripción" name="description">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-file-text-o"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="status_id" class="form-control m-select2"  id="status">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                    <input type="text" id="limitdate" class=" form-control form-control-danger m-input datepicker"  name="limitdate" placeholder="Fecha límite">
                            </div>
                        </div>
                        
                    </div>
                       
                </div>
                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-info m-btn--pill m-btn--air">Registrar</button>
                        <button type="reset" class="btn btn-outline-danger m-btn--pill m-btn--air">Cancelar</button>
                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@if($crud->r)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-users"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                        Tareas Registradas
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <table id="dt" class="table table-striped- table-bordered table-hover table-checkable">

                </table>
                <div class="m_datatable">

                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div id="modalContainer">

</div>

@stop
