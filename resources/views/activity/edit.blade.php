<!--begin::Modal-->
<div class="modal fade" id="eModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Editar Registro
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="editForm">
                <div class="modal-body">
                        @csrf
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" value="{{$data->name}}" class=" form-control form-control-danger m-input" required placeholder="Nombre" name="name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" value="{{$data->description}}" class=" form-control form-control-danger m-input" required placeholder="Descripción" name="description">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-edit"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group m-form__group">
                                  <div class="m-input-icon m-input-icon--left">
                                    <select name="user_id" class="sSpecial form-control m-select2" required>
                                      <option value=""></option>
                                        @foreach($users as $user)
                                          <option {{$data->user_id===$user->id ? 'selected' : ''}} value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>                                
                                  </div>
                              </div>
                          </div>
                        </div>
                        <span></span>
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="status_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($statuses as $status)
                                      <option {{$data->status_id===$status->id ? 'selected' : ''}} value="{{$status->id}}">{{$status->status}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="priority_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($priorities as $priority)
                                      <option {{$data->priority_id===$priority->id ? 'selected' : ''}} value="{{$priority->id}}">{{$priority->priority}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <input type="date" value="{{$data->requestdate}}" class=" form-control form-control-danger m-input" required  name="requestdate">
                            </div>
                          </div>
                        </div>  
                        <span></span>
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <input type="date" value="{{$data->limitdate}}" class=" form-control form-control-danger m-input" required  name="limitdate">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <input type="date" value="{{$data->accepteddate}}" class=" form-control form-control-danger m-input" required  name="accepteddate">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="request_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($requests as $request)
                                      <option {{$data->request_id===$request->id ? 'selected' : ''}} value="{{$request->id}}">{{$request->request_type}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <span></span>
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="sla_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($slas as $sla)
                                      <option {{$data->sla_id===$sla->id ? 'selected' : ''}} value="{{$sla->id}}">{{$sla->description}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="organization_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($organizations as $organization)
                                      <option {{$data->organization_id===$organization->id ? 'selected' : ''}} value="{{$organization->id}}">{{$organization->name}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="subcategory_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($subcategories as $subcategory)
                                      <option {{$data->subcategory_id===$subcategory->id ? 'selected' : ''}} value="{{$subcategory->id}}">{{$subcategory->subcategory}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group m-form__group">
                              <div class="m-input-icon m-input-icon--left">
                                <select name="deparment_id" class="sSpecial form-control m-select2" required>
                                  <option value=""></option>
                                    @foreach($departments as $department)
                                      <option {{$data->department_id===$department->id ? 'selected' : ''}} value="{{$department->id}}">{{$department->name}}</option>
                                    @endforeach
                                </select>                                
                              </div>
                            </div>
                          </div>
                        </div>     
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success m-btn--pill m-btn--air">
                        Editar Registro
                    </button>
                    <button type="button" class="btn btn-outline-danger m-btn--pill m-btn--air" data-dismiss="modal">
                            Cancelar
                        </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
