@extends('layouts.admin')
@section('scripts')
<script src="/js/activity.js"></script>
@stop
@section('content')
@if($crud->c)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="la la-book"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Crear Nueva Actividad
                        </h3>
                    </div>
                </div>
            </div>
            <form action="" id="new">
                <div class="m-portlet__body">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class=" form-control form-control-danger m-input" required placeholder="Nombre de la Actividad" name="name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-edit"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class=" form-control form-control-danger m-input" required placeholder="Descripción" name="description">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-file-text-o"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="user_id" class="form-control m-select2" required id="user">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="status_id" class="form-control m-select2" required id="status">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="priority_id" class="form-control m-select2" required id="priority">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                 <!-- <div class="m-input-icon m-input-icon--left"> -->
                                    <input type="text" id="requestdate" class=" form-control form-control-danger m-input datepicker"  required name="requestdate" autocomplete="off"placeholder="Fecha requerida">
                                    <!-- <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span> -->
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                 <!-- <div class="m-input-icon m-input-icon--left"> -->
                                    <input type="text" id="limitdate" class=" form-control form-control-danger m-input datepicker" required name="limitdate" placeholder="Fecha límite">
                                    <!-- <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span> -->
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                 <!-- <div class="m-input-icon m-input-icon--left"> -->
                                    <input type="text" class=" form-control form-control-danger m-input datepicker" required name="accepteddate" placeholder="Fecha de aceptación">
                                    <!-- <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-user"></i>
                                        </span>
                                    </span> -->
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="request_id" class="form-control m-select2" required id="request">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <span></span>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="sla_id" class="form-control m-select2" required id="sla">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="organization_id" class="form-control m-select2" required id="organization">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="subcategory_id" class="form-control m-select2" required id="subcategory">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <select name="department_id" class="form-control m-select2" required id="department">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>       
                </div>
                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-info m-btn--pill m-btn--air">Registrar</button>
                        <button type="reset" class="btn btn-outline-danger m-btn--pill m-btn--air">Cancelar</button>
                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@if($crud->r)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-users"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                        Actividades Registradas
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <table id="dt" class="table table-striped- table-bordered table-hover table-checkable">

                </table>
                <div class="m_datatable">

                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div id="modalContainer">

</div>

@stop
