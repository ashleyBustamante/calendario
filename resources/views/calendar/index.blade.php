@extends('layouts.admin')
@section('scripts')
<script src="/js/calendar.js"></script>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-calendar"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                        Calendario de Eventos
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <form action="" id="new">
                <div class="m-portlet__body">
                    <div id="calendar"></div>
                </div>
               
            </form>
        </div>
    </div>
</div>

<div id="modalContainer">

</div>
@stop