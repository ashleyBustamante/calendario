@extends('layouts.admin')
@section('scripts')
<link href="/css/roles.css" rel="stylesheet" type="text/css" />
<script src="/assets/vendors/custom/serialize/jquery.serialize-object.min.js" type="text/javascript"></script>
<script src="/assets/vendors/custom/mustache/mustache.js"></script>
<script src="/js/roles.js"></script>
@stop
@section('content')
@if($crud->c)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-lock"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Nuevo Rol
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <form action="" id="submitForm">
                <div class="m-portlet__body">
                    
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">    
                                    <input type="text" class=" form-control form-control-danger m-input" required placeholder="Nombre del Rol" name="name">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-lock"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group">
                                <div class="m-input-icon m-input-icon--left">    
                                    <input type="text" class=" form-control form-control-danger m-input" required placeholder="Descripcion" name="description">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
                                        <span>
                                            <i class="la la-info"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group m-form__group">
                                <label for="menu">
                                    Permisos
                                </label>
                                <div class="row" id="menus">

                                </div>
                            </div>
                        </div>
                    </div>
                    <span></span>
                </div>
                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-info m-btn--pill m-btn--air">Registrar</button>
                        <button type="reset" class="btn btn-outline-danger m-btn--pill m-btn--air">Cancelar</button>
                        
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@if($crud->r)
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-users"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Roles Registrados
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                <i class="la la-angle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                    <table id="dt" class="table table-striped- table-bordered table-hover table-checkable">

                    </table>
                <div class="m_datatable">
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div id="modalContainer">

</div>

@stop
