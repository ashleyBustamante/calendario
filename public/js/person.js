//Inicializacion
var mod = 'person';
$(document).ready(function() {
    dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    $(".dtpck").datepicker();
    validations();
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del); 
    
    $('#dt').on('change', '.vote', vote);
    $('#dt').on('click', '.smph', semaphore);
});

//Definición de Tabla
var Kcolumns = [{
    data: 'id',
    title: '#'
}, {
    data: 'first_name',
    title: 'Nombre'
}, {
    data: 'middle_name',
    title: 'A. Paterno'
}, {
    data: 'last_name',
    title: 'A. Materno'
}, {
    data: 'gender',
    title: 'Género'
}, {
    data: 'elector_id',
    title: 'Clave Elector'
}, {
    data: 'semaphore',
    title: 'Semáforo',
    render: function(r, s, d, u) {
        var x = [
            '<span class="smph ttip m-badge m-badge--secondary" style="cursor:pointer" data-container="body" data-color="0" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "No confianza"></span>',
            '<span class="smph ttip m-badge m-badge--secondary" style="cursor:pointer" data-container="body" data-color="1" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Media confianza"></span>',
            '<span class="smph ttip m-badge m-badge--secondary" style="cursor:pointer" data-container="body" data-color="2" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Alta confianza"></span>'
        ];
        switch (r) {
            case 0:
                x[0] = '<span class="smph ttip m-badge m-badge--danger" style="cursor:pointer" data-color="0" data-container="body" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "No confianza"></span>';
                break;
            case '1':
                x[1] = '<span class="smph ttip m-badge m-badge--warning" style="cursor:pointer" data-color="1" data-container="body" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Media confianza"></span>';
                break;
            case '2':
                x[2] = '<span class="smph ttip m-badge m-badge--success" style="cursor:pointer" data-color="2" data-container="body" data-id="' + d.id + '" data-toggle="m-tooltip" data-placemente="top" data-title = "Alta confianza"></span>';
                break;
        }
        return x[0] + x[1] + x[2];
    }
}, {
    data: 'vote',
    title: 'Voto',
    render: function(r, s, d, u) {
        var c = r == '1' ? 'checked="checked"' : '';
        return '<span class="m-switch m-switch--outline m-switch--icon m-switch--accent" >' +
            '<label>' +
            '<input type="checkbox" ' + c + ' name="" data-id="' + d.id + '" class="vote">' +
            '<span></span>' +
            '</label>' +
            '</span>';
    }
}, {
    data: 'p',
    title: 'Acciones',
    render: renderActions
}];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/role',
        dataType: 'json',
        success: function(e) {
            $('#role').select2({
                placeholder: 'Seleccione un Rol de Usuario',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Registro Exitoso', 'success');
                    dt.ajax.reload();
                    form[0].reset();
                },
                error: eHandler
            });
        }
    });
}

//Semaforo
function semaphore() {
    sid = $(this).data('id');
    color = $(this).data('color');
    $.ajax({
        url: '/admin/' + mod + '/' + sid + '/' + color,
        dataType: 'json',
        success: function(e) {
            ShowNotification('Semáforo cambiado exitosamente', 'success');
            dt.ajax.reload();
        },
        error: eHandler
    });
}

//Voto
function vote() {
    vid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + vid + '/vote',
        dataType: 'json',
        success: function(e) {
            ShowNotification('Voto cambiado exitosamente', 'success');
        },
        error: eHandler
    });
}


//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".dtpck").datepicker();
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    dt.ajax.reload();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                dt.ajax.reload();
            },
            error: eHandler
        });
    }
}