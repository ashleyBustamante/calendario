$(document).ready(function() {
    $('#report').select2({
        placeholder: 'Seleccione el tipo de reporte'
    });
    getStatus();
    
});
var Kcolumns = [{
        data: 'name',
        title: 'Actividad'
    },{
        data: 'description',
        title: 'SLA'
    },{
        data: 'S',
        title: 'Cantidad'
    }
];

function getStatus(){
    $.ajax({
        url: '/admin/status',
        dataType: 'json',
        success: function(e) {
            $('#report').select2({
                placeholder: 'Seleccione un Reporte',
                data: $.map(e.data, function(e) {
                    e.text = 'Reporte por '+e.status;
                    return e;
                })
            });
            $('#report').on('change',function(e){
                getGraph($('#report').val());
            });
        },
        error: eHandler
    });
}

function getGraph(id){
    $.ajax({
        url: '/admin/graph1/'+id,
        dataType: 'json',
        success: function(e) {
            Graph(e.series,e.departments,e.slas);
            dataTableLocal($('#tbl'),e.results,Kcolumns);
        },
        error: eHandler
    });
}

function Graph(series,departments,slas) {

    var myChart = echarts.init(document.getElementById('graph'));


    option = {
        //color: ['#003366', '#006699', '#4cabce'],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: departments
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            left: 'right',
            top: 'center',
            feature: {
                mark: { show: true },
                magicType: { show: true, type: ['line', 'bar', 'stack', 'tiled'],
                title:{
                    line:'Lineal',
                    bar:'Barras',
                    stack:'Barras Apiladas',
                    tiled:'Barras'
                } 
            },
                saveAsImage: { show: true,title:'Guardar' }
            }
        },
        calculable: true,
        xAxis: [{
            type: 'category',
            axisTick: { show: false },
            data: slas
        }],
        yAxis: [{
            type: 'value'
        }],
        series:series
    };
    myChart.setOption(option);
}

function dataTableLocal(tbl, data, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: data,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}