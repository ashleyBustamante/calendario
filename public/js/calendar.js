$(document).ready(function(){
	//renderCalendar();
    getEvents();
});


function getEvents(){
    $.ajax({
        url: '/admin/calendar/events',
        dataType: 'json',
        success: function(e) {
            renderCalendar(e);
        },
        error: eHandler
    });
}

var calendar;

function renderCalendar(events){
	calendar = $('#calendar').fullCalendar({
		locale:'es',
		events:events,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listWeek'
		},
		week: {
            dow: 0,
            doy: 6 // The week that contains Jan 1st is the first week of the year.
        },
        buttonText: {
            prev: "Ant",
            next: "Sig",
            today: "Hoy",
            month: "Mes",
            week: "Semana",
            day: "Día",
            list: "Agenda"
        },
        weekLabel: "Sm",
        allDayHtml: "Todo<br/>el día",
        eventLimitText: "más",
        noEventsMessage: "No hay eventos para mostrar",
		editable: false,
        eventClick: function(e) {
            showEvent(e);
        }
        });
}

var edid;
var mod;

function showEvent(event){
    edid = event._id;
    mod = event.type == 1 ? 'activity':'task';
    edt();
}

function edt() {
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    getData();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}
/*$(document).ready(function() {
    setCalendar();
    $('#new').click(showEvent);

    drop1();
});

function drop1() {
    $.ajax({
        url: '/admin/departament',
        dataType: 'json',
        success: function(e) {
            $('#departaments').select2({
                width: '200',
                placeholder: 'Departamento',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
            $('#departaments').on('change', function() {
                $('#dep').val($('#departaments').val());
                cal.fullCalendar('refetchEvents');
                if ($('#inv').val() == 2 && $('#dep').val() != $('#Odep').val()) {
                    cal.fullCalendar('option', 'editable', false);
                } else {
                    cal.fullCalendar('option', 'editable', true);
                }
            });
        },
        error: eHandler
    });
}

function showEvent(id = 0) {
    if ($('#inv').val() == 2 && $('#dep').val() != $('#Odep').val()) {
        ShowNotification('No tiene permiso para hacer esta acción', 'error');
    } else if ($('#inv').val() == 3) {
        ShowNotification('No tiene permiso para hacer esta acción', 'error');
    } else {

        $.ajax({
            url: '/admin/event/' + id + '/' + $('#dep').val(),
            dataType: 'json',
            success: function(e) {
                $("#modalContainer").html(e.view);

                $("#eModal").modal();
                $(".dt").datetimepicker({
                    locale: 'es'
                });
                newEvent();
            },
            error: eHandler
        });
    }
}

function newEvent() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            var id = parseInt($('#del').val());
            if (id == 0) {
                form.ajaxSubmit({
                    url: '/admin/event',
                    method: 'POST',
                    data: { allDay: document.getElementById('allDay').checked ? 1 : 0 },
                    success: function(response, status, xhr, $form) {
                        ShowNotification('Evento ingresado con éxito', 'success');
                        $("#eModal").modal('toggle');
                        cal.fullCalendar('refetchEvents');
                    },
                    error: eHandler
                });
            } else {
                form.ajaxSubmit({
                    url: '/admin/event/' + id,
                    method: 'POST',
                    data: { _method: 'DELETE' },
                    success: function(response, status, xhr, $form) {
                        ShowNotification('Evento Eliminado con éxito', 'success');
                        $("#eModal").modal('toggle');
                        cal.fullCalendar('refetchEvents');
                    },
                    error: eHandler
                });
            }
        }
    });
}

function editEvent(ev) {
    var t = ev.end == null ? '2018-12-12 00:00:00' : ev.end.format('YYYY-MM-DD hh:mm:ss');
    var event = {
        start: ev.start.format('YYYY-MM-DD hh:mm:ss'),
        end: t,
        _method: 'PUT',
        _token: $('meta[name="csrf-token"]').attr('content')
    };
    $.ajax({
        url: '/admin/event/' + ev.id,
        data: event,
        method: 'POST',
        dataType: 'json',
        success: function(e) {
            ShowNotification('Evento Actualizado con éxito', 'success');
            //cal.fullCalendar('refetchEvents');
        },
        error: eHandler
    });
}
var cal;

function setCalendar() {
    var editable = $('#inv').val() != 3;

    cal = $("#calendar").fullCalendar({
        locale: 'es',
        editable: editable,
        header: {
            left: "prev,next today",
            center: "title",
            right: "month,agendaWeek,agendaDay,listWeek"
        },
        eventSources: [{
            url: '/admin/events/',
            type: 'POST',
            data: function() {
                return { id: $('#dep').val(), _token: $('meta[name="csrf-token"]').attr('content') };
            },
            error: function() {
                alert('there was an error while fetching events!');
            }
        }],
        eventDataTransform: function(e) {
            e.allDay = e.allDay == 0 ? false : true;
            if (e.allDay) {
                e.end = '';
            }
            return e;
        },
        eventClick: function(e) {
            showEvent(e.id);
        },
        eventDrop: function(e) {
            editEvent(e);
        },
        eventResize: function(e) {
            editEvent(e);
        },
        eventRender: function(e, t) {
            t.hasClass("fc-day-grid-event") ? (t.data("content", e.description),
                t.data("placement", "top"),
                mApp.initPopover(t)) : t.hasClass("fc-time-grid-event") ? t.find(".fc-title").append('<div class="fc-description">' + e.description + "</div>") : 0 !== t.find(".fc-list-item-title").lenght && t.find(".fc-list-item-title").append('<div class="fc-description">' + e.description + "</div>")
        }
    });
}*/