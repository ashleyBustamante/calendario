//Inicializacion
var mod = 'activity';
$(document).ready(function() {
    //dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    $('.datepicker').datepicker({
        format:'yyyy-mm-dd'
    });
    
    drop0();
    drop1();
    drop2();
    drop3();
    drop4();
    drop5();
    drop6();
    drop7();
    drop8();
    validations();
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del);
});

//Definición de Tabla
var Kcolumns = [{
        data: 'id',
        title: '#'
    }, {
        data: 'name',
        title: 'Nombre'
    }, {
        data: 'description',
        title: 'Descripción'
    }, {
        data: 'user.name',
        title: 'Nombre'
    }, {
        data: 'status.status',
        title: 'Estado'
    }, {
        data: 'priority.priority',
        title: 'Descripcion'
    }, {
        data: 'requestdate',
        title: 'Fecha Solicitada'
    }, {
        data: 'limitdate',
        title: 'Fecha Límite'
    }, {
        data: 'accepteddate',
        title: 'Fecha Solicitada'
    }, {
        data: 'request.request_type',
        title: 'Via'
    }, {
        data: 'sla.description',
        title: 'SLA'
    }, {
        data: 'organization.name',
        title: 'Organización'
    }, {
        data: 'subcategory.subcategory',
        title: 'Subcategoría'
    },
    {
        data: 'department.name',
        title: 'Departamento'
    },
    {
        data: 'p',
        title: 'Acciones',
        render: renderActions
    }
];

//Getters

function drop0() {
    $.ajax({
        url: '/admin/activity',
        dataType: 'json',
        success: function(e) {
            dataTableLocal($('#dt'), e.data, Kcolumns);
        },
        error: eHandler
    });
}

function drop1() {
    $.ajax({
        url: '/admin/user',
        dataType: 'json',
        success: function(e) {
            $('#user').select2({
                placeholder: 'Seleccione un Usuario',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop2() {
    $.ajax({
        url: '/admin/status',
        dataType: 'json',
        success: function(e) {
            $('#status').select2({
                placeholder: 'Seleccione un Estado',
                data: $.map(e.data, function(e) {
                    e.text = e.status;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop3() {
    $.ajax({
        url: '/admin/priority',
        dataType: 'json',
        success: function(e) {
            $('#priority').select2({
                placeholder: 'Seleccione una Prioridad',
                data: $.map(e.data, function(e) {
                    e.text = e.priority;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop4() {
    $.ajax({
        url: '/admin/request_type',
        dataType: 'json',
        success: function(e) {
            $('#request').select2({
                placeholder: 'Seleccione una Vía de Solicitud',
                data: $.map(e.data, function(e) {
                    e.text = e.request_type;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop5() {
    $.ajax({
        url: '/admin/slas',
        dataType: 'json',
        success: function(e) {
            $('#sla').select2({
                placeholder: 'Seleccione SLA',
                data: $.map(e.data, function(e) {
                    e.text = e.description;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop6() {
    $.ajax({
        url: '/admin/organization',
        dataType: 'json',
        success: function(e) {
            $('#organization').select2({
                placeholder: 'Seleccione una Organización',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop7() {
    $.ajax({
        url: '/admin/subcategory',
        dataType: 'json',
        success: function(e) {
            $('#subcategory').select2({
                placeholder: 'Seleccione una Subcategoría',
                data: $.map(e.data, function(e) {
                    e.text = e.subcategory;
                    return e;
                })
            });
        },
        error: eHandler
    });
}
function drop8() {
    $.ajax({
        url: '/admin/department',
        dataType: 'json',
        success: function(e) {
            $('#department').select2({
                placeholder: 'Seleccione un departamento',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Registro Exitoso', 'success');
                    drop0();
                    //dt.ajax.reload();
                    form[0].reset();
                },
                error: eHandler
            });
        }
    });
}

//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un solicitante', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    drop0();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                drop0();
            },
            error: eHandler
        });
    }
}

function dataTableLocal(tbl, data, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: data,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}
