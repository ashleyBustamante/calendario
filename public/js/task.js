//Inicializacion
var mod = 'task';
$(document).ready(function() {
    //dataTable($('#dt'), '/admin/' + mod, Kcolumns);
    $('.datepicker').datepicker({
        format:'yyyy-mm-dd'
    });
    drop1();
    drop2();
    drop3();  
    drop4();
    validations();
    
    $('#dt').on('click', '.bActive', function() {});
    $('#dt').on('click', '.bEdit', edt);
    $('#dt').on('click', '.bDelete', del);
});

var id;

function getData(){
    $.ajax({
        url: '/admin/ftask/'+id,
        dataType: 'json',
        success: function(e) {
            dataTableLocal($('#dt'),e.data,Kcolumns);
        }
    });
}

//Definición de Tabla
var Kcolumns = [{
        data: 'id',
        title: '#'
    }, {
        data: 'user.name',
        title: 'Usuario'
    },{
        data: 'activity.name',
        title: 'Actividad'
    },{
        data: 'description',
        title: 'Descripción'
    },{
        data: 'status.status',
        title: 'Estado'
    },{
        data: 'limitdate',
        title: 'Fecha límite'
    },
    {
        data: 'p',
        title: 'Acciones',
        render: renderActions
    }
];

//Getters

function drop1() {
    $.ajax({
        url: '/admin/user',
        dataType: 'json',
        success: function(e) {
            $('#user').select2({
                placeholder: 'Seleccione un usuario',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        }
    });
}

function drop2() {
    $.ajax({
        url: '/admin/activity',
        dataType: 'json',
        success: function(e) {
            $('#activity').select2({
                placeholder: 'Seleccione una actividad',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
            $('#activity').on('change',function(e){
                id=$('#activity').val();
                getData();
            });
        }
    });
}
function drop3() {
    $.ajax({
        url: '/admin/status',
        dataType: 'json',
        success: function(e) {
            $('#status').select2({
                placeholder: 'Seleccione un estado',
                data: $.map(e.data, function(e) {
                    e.text = e.status;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function drop4() {
    $.ajax({
        url: '/admin/template',
        dataType: 'json',
        success: function(e) {
            $('#template').select2({
                placeholder: 'Seleccione una Plantilla',
                data: $.map(e.data, function(e) {
                    e.text = e.name;
                    return e;
                })
            });
        },
        error: eHandler
    });
}

function validations() {
    $('#new').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#new');
            form.ajaxSubmit({
                url: '/admin/' + mod,
                method: 'POST',
                success: function(response, status, xhr, $form) {
                    ShowNotification('Registro Exitoso', 'success');
                    getData();
                    form[0].reset();
                },
                error: eHandler
            });
        }
    });
}

//Edición

var edid;

function edt() {
    edid = $(this).data('id');
    $.ajax({
        url: '/admin/' + mod + '/' + edid + '/edit',
        dataType: 'json',
        success: function(e) {
            $("#modalContainer").html(e.view);
            $("#eModal").modal();
            $(".sSpecial").select2({ placeholder: 'Seleccione un Rol de Usuario', width: '100%' });
            update();
        },
        error: eHandler
    });
}

function update() {
    $('#editForm').validate({
        errorPlacement: ePlacement,
        errorClass: eClass,
        submitHandler(frm) {
            var form = $('#editForm');
            form.ajaxSubmit({
                url: '/admin/' + mod + '/' + edid,
                method: 'POST',
                data: { _method: 'PUT' },
                success: function(response, status, xhr, $form) {
                    ShowNotification('Actualización Exitosa', 'success');
                    getData();
                    $("#eModal").modal('toggle');
                },
                error: eHandler
            });
        }
    });
}

//Eliminación

var delid;

function del() {
    delid = $(this).data('id');
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "No habrá manera de revertir esta acción",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Si, Elimínalo",
        cancelButtonText: "Cancelar",
        confirmButtonClass: "btn btn-danger m-btn--pill m-btn--air",
        cancelButtonClass: "btn btn-outline-default m-btn--pill m-btn--air"
    }).
    then(delHandler);
}

function delHandler(r) {
    if (r.value) {
        $.ajax({
            url: '/admin/' + mod + '/' + delid,
            method: 'POST',
            data: { _token: $("[name='_token']").val(), _method: 'DELETE' },
            dataType: 'json',
            success: function(e) {
                ShowNotification('Registro Eliminado Satisfactoriamente', 'success');
                getData();
            },
            error: eHandler
        });
    }
}

function dataTableLocal(tbl, data, columns) {
    dt = tbl.DataTable({
        language: {
            processing: "Procesando...",
            search: "Buscar&nbsp;:",
            lengthMenu: "Mostrar _MENU_ ",
            info: "Mostrando de _START_ al _END_ de un total de _TOTAL_ registros",
            infoEmpty: "No hay datos disponibles",
            infoFiltered: "(Filtrando _MAX_ registros)",
            infoPostFix: "",
            loadingRecords: "Obteniendo Datos...",
            zeroRecords: "No hay datos disponibles para su búsqueda",
            emptyTable: "No hay datos disponibles",
            aria: {
                sortAscending: ": Ordenar Ascendente",
                sortDescending: ": Ordenar Descendente"
            }
        },
        data: data,
        destroy: true,
        responsive: true,
        dom: "<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>\n\t\t\t<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        buttons: [
            { extend: "print", text: "Imprimir" },
            { extend: "copyHtml5", text: "Copiar" },
            "excelHtml5",
            "pdfHtml5"
        ],
        columns: columns,
        drawCallback: function() {
            $(".ttip").tooltip();
        }
    });

    return dt;
}